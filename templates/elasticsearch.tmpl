"use strict";

const Env = use("Env");

module.exports = {
  /*
  |--------------------------------------------------------------------------
  | Default Connection
  |--------------------------------------------------------------------------
  |
  | Connection defines the default connection settings to be used while
  | interacting with Elasticsearch.
  |
  */
  connection: Env.get("ES_CONNECTION", "primary"),

  connections: {
    /*
    |--------------------------------------------------------------------------
    | Primary Connection
    |--------------------------------------------------------------------------
    |
    | Here we define connection settings for primary connection
    |
    |
    */
    primary: {
      debug: Env.get("ES_DEBUG") === "true",
      indexName: Env.get("ES_PRIMARY_INDEX"),
      waitForRefresh: true,
      versionIncrement: true,
      connection: {
        keepAlive: true,
        httpAuth: Env.get("ES_HTTP_AUTH"),
        hosts: Env.get("ES_PRIMARY_HOSTS", "localhost:9200").split(",")
        deadTimeout: Env.get("ES_PRIMARY_RECONNECT_INTERVAL", 3000)
      },
      documents: {
        // Example: "example" 
      }
    }
  }
};
