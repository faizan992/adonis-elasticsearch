"use strict";

const _ = require("lodash");
const BaseQueryBuilder = require("./Base");
const Aggregator = require("./Aggregator");
const Preferences = require("./Preferences");

class QueryBuilder extends BaseQueryBuilder {
  constructor() {
    super();
    this._selectAttributes = [];
    this._excludeAttributes = [];
    this._aggregation = new Aggregator();
    this._preferences = new Preferences();
    return new Proxy(this, this);
  }

  /**
   * Returns prepared DSL
   *
   * @returns {Object}
   */
  getDSL() {
    let dsl = {
      ...super.getDSL(),
      ...this._preferences.options
    };
    if (this._selectAttributes.length) {
      _.set(dsl, "_source.includes", _.cloneDeep(this._selectAttributes));
    }
    if (this._excludeAttributes.length) {
      _.set(dsl, "_source.excludes", _.cloneDeep(this._excludeAttributes));
    }
    let aggs = this.aggs();
    if (aggs && !_.isEmpty(aggs.dsl.aggs)) {
      dsl.aggs = aggs.dsl.aggs;
    }
    return dsl;
  }

  /**
   * Magic getter function
   */
  get(target, prop) {
    if (typeof this[prop] !== "undefined") {
      return this[prop];
    }
    if (typeof this.must()[prop] === "function") {
      return function() {
        return this.must()[prop](...arguments);
      };
    }
    if (typeof this._preferences[prop] === "function") {
      return function() {
        return this._preferences[prop](...arguments);
      };
    }
    return this[prop];
  }

  /**
   * This method is used to select source fields
   *
   * @param {Array<String>|String} fields
   * @returns {QueryBuilder}
   */
  select(fields) {
    if (_.isArray(fields)) {
      fields.forEach(field => {
        if (typeof field === "string") {
          this._selectAttributes.push(field);
        }
      });
    } else {
      this._selectAttributes.push(fields);
    }
    return this;
  }

  /**
   * This method is used to exclude source fields
   *
   * @param {Array<String>|String} fields
   * @returns {QueryBuilder}
   */
  exclude(fields) {
    if (_.isArray(fields)) {
      fields.forEach(field => {
        if (typeof field === "string") {
          this._excludeAttributes.push(field);
        }
      });
    } else {
      this._excludeAttributes.push(fields);
    }
    return this;
  }

  /**
   * This method is used to get aggregator for dsl query
   *
   * @returns {Object}
   */
  aggs() {
    return this._aggregation;
  }
}

module.exports = QueryBuilder;
