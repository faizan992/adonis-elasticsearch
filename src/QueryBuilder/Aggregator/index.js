"use strict";

const _ = require("lodash");
const BucketAggregator = require("./Bucket");
const MatrixAggregator = require("./Matrix");
const MetricsAggregator = require("./Metrics");
const PipelineAggregator = require("./Pipeline");

class Aggregator {
  constructor() {
    this._aggs = {};
    this._BucketAgg = new BucketAggregator(this);
    this._MatrixAgg = new MatrixAggregator(this);
    this._MetricsAgg = new MetricsAggregator(this);
    this._PipelineAgg = new PipelineAggregator(this);
    return new Proxy(this, this);
  }

  /**
   * Magic getter function for this aggregator
   */
  get(target, prop) {
    let self = this;
    let aggregators = [
      "_BucketAgg",
      "_MatrixAgg",
      "_MetricsAgg",
      "_PipelineAgg"
    ];
    let method;
    for (let i = 0; i < aggregators.length; i++) {
      if (typeof this[aggregators[i]][prop] === "function") {
        method = function() {
          self[aggregators[i]][prop](...arguments);
        };
        break;
      }
    }
    if (typeof method === "function") {
      return method;
    }
    return this[prop];
  }

  /**
   * Returns aggregation dsl
   */
  get dsl() {
    let aggregations = {
      aggs: {}
    };
    for (let key in this._aggs) {
      aggregations.aggs[key] = _.omit(this._aggs[key], ["aggs"]);
      if (this._aggs[key].aggs) {
        aggregations.aggs[key].aggs = this._aggs[key].aggs.dsl.aggs;
      }
    }
    return _.cloneDeep(aggregations);
  }

  /**
   * This method is used to add aggregator
   *
   * @param {String} name
   * @param {Object} aggs
   * @param {Function} [nested]
   * @returns {Aggregator}
   */
  aggs(name, aggs, nested) {
    if (typeof name === "string" && name && !_.isEmpty(aggs)) {
      this._aggs[name] = aggs;
      if (nested && typeof nested === "function") {
        let nestedAggregator = new Aggregator();
        nested(nestedAggregator);
        this._aggs[name].aggs = nestedAggregator;
      }
    }
    return this;
  }
}

module.exports = Aggregator;
