"use strict";

/**@typedef {import('./index')} Aggregator */
const _ = require("lodash");

class MetricsAggregator {
  /**
   * Returns those aggregations which accepts :
   *  - field as string or script object
   *  - object for extra properties (Optional)
   */
  get typeA() {
    return [
      "avg",
      "max",
      "min",
      "sum",
      "stats",
      "geoBounds",
      "valueCount",
      "percentiles",
      "cardinality",
      "geoCentroid",
      "extendedStats",
      "medianAbsoluteDeviation"
    ];
  }

  /**
   * Returns those aggregations which methods are explicitly defined
   *
   */
  get typeB() {
    return ["weightedAvg", "scriptedMetric", "topHits"];
  }

  /**
   * @param {Aggregator} Aggregator
   */
  constructor(Aggregator) {
    this._Aggregator = Aggregator;
    return new Proxy(this, this);
  }

  /**
   * Magic getter function for metrics aggregation
   */
  get(target, prop) {
    let self = this;
    if (this.typeA.indexOf(prop) > -1) {
      return function() {
        let agg = _.snakeCase(prop);
        let name = arguments[0];
        let field = arguments[1];
        let extra = arguments[2];
        if (!field) {
          return self._Aggregator;
        }
        let aggs = { [agg]: {} };
        if (typeof field === "string") {
          aggs[agg].field = field;
        } else if (typeof field === "object" && field.script) {
          aggs[agg].script = field.script;
        } else {
          return self._Aggregator;
        }
        if (extra && typeof extra === "object") {
          _.merge(aggs[agg], _.omit(extra, ["aggs"]));
        }
        return self._Aggregator.aggs(name, aggs);
      };
    }
    return this[prop];
  }

  /**
   * This method is used to set weighted avg aggregator
   *
   * @param {String} name
   * @param {Object} value
   * @param {Object} weight
   * @return {Aggregator}
   */
  weightedAvg(name, value, weight) {
    if (!value || !weight) {
      return this._Aggregator;
    }
    let aggs = { weighted_avg: {} };
    if (typeof value === "object") {
      aggs.weighted_avg.value = value;
    }
    if (typeof weight === "object") {
      aggs.weighted_avg.weight = weight;
    }
    if (!_.isEmpty(aggs.weighted_avg)) {
      return this._Aggregator.aggs(name, aggs);
    }
    return this._Aggregator;
  }

  /**
   * This method is used to set scripted metric aggregation
   *
   * @param {String} name
   * @param {Object} scripts
   * @returns {Aggregator}
   */
  scriptedMetric(name, scripts) {
    if (scripts) {
      return this._Aggregator.aggs(name, {
        scripted_metric: scripts
      });
    }
    return this._Aggregator;
  }

  /**
   * This method is used to get top hits aggregation
   *
   * @param {String} name
   * @param {Object} params
   * @returns {Aggregator}
   */
  topHits(name, params) {
    return this._Aggregator.aggs(name, {
      top_hits: params || {}
    });
  }
}

module.exports = MetricsAggregator;
