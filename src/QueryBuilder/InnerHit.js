"use strict";

const _ = require("lodash");
const Preferences = require("./Preferences");

class InnerHit {
  constructor() {
    this._sort = [];
    this._limit = null;
    this._offset = null;
    this._scriptFields = [];
    this._selectAttributes = [];
    this._excludeAttributes = [];
    this._preferences = new Preferences();
    return new Proxy(this, this);
  }

  /**
   * Returns possible preferences
   */
  get PREFERENCE_TYPES() {
    return ["explain", "highlight", "version", "docvalue_fields"];
  }

  /**
   * Magic getter function
   */
  get(target, prop) {
    var preferenceType = _.snakeCase(prop);
    if (typeof this[prop] !== "undefined") {
      return this[prop];
    }
    if (this.PREFERENCE_TYPES.indexOf(preferenceType) >= 0) {
      return function() {
        return this._preferences[prop](...arguments);
      };
    }
  }

  /**
   * Returns inner hit dsl
   */
  getDSL() {
    let dsl = { ...this._preferences.options };
    if (this._limit && !isNaN(this._limit)) {
      dsl.size = this._limit;
    }
    if (this._offset && !isNaN(this._offset)) {
      dsl.from = this._offset;
    }
    if (this._selectAttributes.length) {
      _.set(dsl, "_source.includes", this._selectAttributes);
    }
    if (this._excludeAttributes.length) {
      _.set(dsl, "_source.excludes", this._excludeAttributes);
    }
    if (this._name && typeof this._name === "string") {
      dsl.name = this._name;
    }
    return dsl;
  }

  /**
   * This method is used to set name for inner hit
   *
   * @param {String} name
   * @returns {InnerHit}
   */
  name(name) {
    this._name = name;
    return this;
  }

  /**
   * This method is used to add limit to query result
   *
   * @param {Number} size
   * @returns {InnerHit}
   */
  limit(size) {
    this._limit = size;
    return this;
  }

  /**
   * This method is used to add offset to query result
   *
   * @param {Number} from
   * @returns {InnerHit}
   */
  offset(from) {
    this._offset = from;
    return this;
  }

  /**
   * This method is used to apply sort criteria
   *
   * @param {String} field
   * @param {String} [condition='asc']
   * @returns {InnerHit}
   */
  sort(field, condition = "asc") {
    this._sort.push({ [field]: condition });
    return this;
  }

  /**
   * This method is used to select source fields
   *
   * @param {Array<String>|String} fields
   * @returns {InnerHit}
   */
  select(fields) {
    if (_.isArray(fields)) {
      fields.forEach(field => {
        if (typeof field === "string") {
          this._selectAttributes.push(field);
        }
      });
    } else {
      this._selectAttributes.push(fields);
    }
    return this;
  }

  /**
   * This method is used to exclude source fields
   *
   * @param {Array<String>|String} fields
   * @returns {InnerHit}
   */
  exclude(fields) {
    if (_.isArray(fields)) {
      fields.forEach(field => {
        if (typeof field === "string") {
          this._excludeAttributes.push(field);
        }
      });
    } else {
      this._excludeAttributes.push(fields);
    }
    return this;
  }

  /**
   * This method is used to add script field
   *
   * @param {String} field
   * @param {Object|String} script
   * @returns {InnerHit}
   */
  scriptField(field, script) {
    this._scriptFields.push({
      [field]: { script }
    });
    return this;
  }
}

module.exports = InnerHit;
