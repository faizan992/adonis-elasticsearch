"use strict";

const _ = require("lodash");

class Preferences {
  constructor() {
    this._preferences = {};
    return new Proxy(this, this);
  }

  get TYPES() {
    return [
      "rescore",
      "explain",
      "version",
      "collapse",
      "highlight",
      "min_score",
      "preference",
      "search_type",
      "search_after",
      "indices_boost",
      "stored_fields",
      "docvalue_fields"
    ];
  }

  /**
   * Magic getter function
   */
  get(target, prop) {
    var preferenceType = _.snakeCase(prop);
    if (this.TYPES.indexOf(preferenceType) >= 0) {
      return function() {
        return this.setPreference(prop, ...arguments);
      };
    }
  }

  /**
   * Returns defined preferences
   */
  get options() {
    return this._preferences;
  }

  /**
   * This method is used to preference
   */
  setPreference(name, preference) {
    this._preferences[name] = preference;
  }
}

module.exports = Preferences;
