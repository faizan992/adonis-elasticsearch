"use strict";

const _ = require("lodash");
const FilterQuery = require("./FilterQuery");

class BaseQueryBuilder {
  constructor() {
    if (this.constructor.name === "BaseQueryBuilder") {
      throw Error("Base class can't be instantiated");
    }
    this._dsl = {};
    this._sort = [];
    this._limit = null;
    this._offset = null;
    this._scriptFields = [];
    this._mustConditions = new FilterQuery();
    this._shouldConditions = new FilterQuery();
    this._filterConditions = new FilterQuery();
  }

  /**
   * Returns prepared DSL
   *
   * @returns {Object}
   */
  getDSL() {
    let dsl = {
      query: {}
    };
    if (this._limit && !isNaN(this._limit)) {
      dsl.size = this._limit;
    }
    if (this._offset && !isNaN(this._offset)) {
      dsl.from = this._offset;
    }
    if (this._sort.length) {
      if (this._sort.length === 1) {
        dsl.sort = this._sort[0];
      } else {
        dsl.sort = _.cloneDeep(this._sort);
      }
    }
    if (this._scriptFields.length) {
      this.script_fields = _.cloneDeep(this.script_fields);
    }
    if (this.must().conditions.length) {
      _.set(dsl, "query.bool.must", _.cloneDeep(this.must().conditions));
    }
    if (this.filter().conditions.length) {
      _.set(dsl, "query.bool.filter", _.cloneDeep(this.filter().conditions));
    }
    if (this.should().conditions.length) {
      _.set(dsl, "query.bool.should", _.cloneDeep(this.should().conditions));
      if (!isNaN(this._minimumShouldMatch)) {
        dsl.query.bool.minimum_should_match = this._minimumShouldMatch;
      }
    }
    if (_.isEmpty(dsl.query.bool)) {
      dsl.query = {
        match_all: {}
      };
    }
    return dsl;
  }

  /**
   * This method is used to add limit to query result
   *
   * @param {Number} size
   * @returns {BaseQueryBuilder}
   */
  limit(size) {
    this._limit = size;
    return this;
  }

  /**
   * This method is used to add offset to query result
   *
   * @param {Number} from
   * @returns {BaseQueryBuilder}
   */
  offset(from) {
    this._offset = from;
    return this;
  }

  /**
   * This method is used to apply sort criteria
   *
   * @param {String} field
   * @param {String} [condition='asc']
   */
  sort(field, condition = "asc") {
    this._sort.push({ [field]: condition });
    return this;
  }

  /**
   * This method is used to add script field
   *
   * @param {String} field
   * @param {Object|String} script
   * @returns {BaseQueryBuilder}
   */
  scriptField(field, script) {
    this._scriptFields.push({
      [field]: { script }
    });
    return this;
  }

  /**
   * This method is used to get must query
   *
   * @returns {FilterQuery}
   */
  must() {
    return this._mustConditions;
  }

  /**
   * This method is used to get filter query
   *
   * @returns {FilterQuery}
   */
  filter() {
    return this._filterConditions;
  }

  /**
   * This method is used to get should query
   *
   * @returns {FilterQuery}
   */
  should() {
    return this._shouldConditions;
  }

  /**
   * This method is used to set minimum should match property
   *
   * @param {Number} minimum
   * @returns {BaseQueryBuilder}
   */
  minimumShouldMatch(minimum) {
    this._minimumShouldMatch = minimum;
    return this;
  }
}

module.exports = BaseQueryBuilder;
