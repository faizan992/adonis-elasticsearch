"use strict";

const OperationMaps = {
  PING: "Ping",
  OPEN_INDEX: "Open Index",
  CLOSE_INDEX: "Close Index",
  CREATE_INDEX: "Create Index",
  DROP_INDEX: "Drop Index",
  UPDATE_MAPPINGS: "Update Mappings",
  UPDATE_SETTINGS: "Update Settings",
  SEARCH: "Search",
  SCROLL: "Scroll",
  CLEAR_SCROLL: "Clear Scroll",
  INDEX: "Index",
  UPDATE: "Update",
  DELETE: "Delete",
  BULK: "Bulk",
  UPDATE_BY_QUERY: "Update By Query",
  DELETE_BY_QUERY: "Delete By Query"
};

class Operation {
  static getName(key) {
    return OperationMaps[key];
  }
}

module.exports = Operation;
