"use strict";
const _ = use("lodash");
const Timer = require("../Lib/Timer");
const Operations = require("./Operations");
const elasticsearch = require("elasticsearch");
/**@typedef {QueryBuilder}*/
const QueryBuilder = require('../QueryBuilder');

class Connection {
  /**
   *
   * @mixin
   * @param {String} name
   * @param {Object} config
   * @param {Boolean} config.debug
   * @param {String} config.indexName
   * @param {Object} config.connection
   * @param {Object} config.documents
   * @param {Object} config.scrollSize
   * @param {Boolean} config.waitForRefresh
   * @param {Boolean} config.versionIncrement
   */
  constructor(name, config) {
    this.name = name;
    this.debug = config.debug;
    this.indexName = config.indexName;
    this.documents = config.documents;
    this.scrollSize = config.scrollSize || 1000;
    this.waitForRefresh = config.waitForRefresh;
    this.versionIncrement = config.versionIncrement;
    this.reconnectInterval = config.connection.deadTimeout;
    this.client = new elasticsearch.Client(config.connection);
  }

  /**
   * This method is used to send request to ES server
   *
   * @param {Promise} requestMethod
   */
  async sendRequest(requestMethod) {
    const resp = await requestMethod;
    if (!resp) {
      return new Error("Unknown Error");
    }
    return resp;
  }

  /**
   * This method is used to ping ES server
   */
  async ping() {
    try {
      const resp = await this.sendRequest(
        this.client.ping({
          requestTimeout: 1000
        })
      );
      return resp;
    } catch (err) {
      this.log({
        err,
        operation: Operations.getName("PING")
      });
      throw err;
    }
  }

  /**
   * This method is used to create index
   *
   * @param {Object} [payload]
   * @param {Object} [payload.settings]
   * @param {Object} [payload.mappings]
   */
  async createIndex(payload = {}) {
    const tick = new Timer();
    tick.start();
    const body = { settings: payload.settings, mappings: payload.mappings };
    try {
      const resp = await this.sendRequest(
        this.client.indices.create({
          index: this.indexName,
          body
        })
      );
      this.log({
        tick,
        body,
        operation: Operations.getName("CREATE_INDEX")
      });
      return resp;
    } catch (err) {
      this.log({
        err,
        tick,
        body,
        operation: Operations.getName("CREATE_INDEX")
      });
      throw err;
    }
  }

  /**
   * This method is used to delete index
   */
  async deleteIndex() {
    const tick = new Timer();
    tick.start();
    try {
      const resp = await this.sendRequest(
        this.client.indices.delete({
          index: this.indexName
        })
      );
      this.log({
        tick,
        operation: Operations.getName("DROP_INDEX")
      });
      return resp;
    } catch (err) {
      this.log({
        err,
        tick,
        operation: Operations.getName("DROP_INDEX")
      });
      throw err;
    }
  }

  /**
   * This method is used to update index mappings
   *
   * @param {String} document
   * @param {Object} body
   * @param {Boolean} [updateAllTypes=false]
   */
  async updateMappings(document, body, updateAllTypes = false) {
    const tick = new Timer();
    tick.start();
    const mappings = {
      index: this.indexName,
      type: document,
      body
    };
    if (updateAllTypes) {
      mappings.updateAllTypes = true;
    }
    try {
      const resp = await this.sendRequest(this.client.indices.putMapping(mappings));
      this.log({
        tick,
        body,
        document,
        operation: Operations.getName("UPDATE_MAPPINGS")
      });
      return resp;
    } catch (err) {
      this.log({
        err,
        tick,
        body,
        document,
        operation: Operations.getName("UPDATE_MAPPINGS")
      });
      throw err;
    }
  }

  /**
   * This method is used to update index settings
   *
   * @param {Object} body
   */
  async updateSettings(body) {
    const tick = new Timer();
    tick.start();
    try {
      let settings = {
        index: this.indexName,
        body: _.pick(body, ["index"])
      };
      if (!_.isEmpty(settings.body)) {
        await this.sendRequest(this.client.indices.putSettings(settings));
        this.log({
          tick,
          body,
          operation: Operations.getName("UPDATE_SETTINGS")
        });
      }
      settings.body = _.omit(body, ["index"]);
      if (_.isEmpty(settings.body)) {
        return true;
      }
      await this.close();
      const resp = await this.sendRequest(this.client.indices.putSettings(settings));
      this.log({
        tick,
        body,
        operation: Operations.getName("UPDATE_SETTINGS")
      });
      this.open();
      return resp;
    } catch (err) {
      this.log({
        err,
        tick,
        body,
        operation: Operations.getName("UPDATE_SETTINGS")
      });
      await this.open();
      throw err;
    }
  }

  /**
   * This method is used to query data with other options
   * @param {String} document
   * @param {Object} DSL
   * @param {Object} options
   */
  async _search(document, DSL, options) {
    const tick = new Timer();
    tick.start();
    try {
      const resp = await this.sendRequest(
        this.client.search({
          ...options,
          index: this.indexName,
          type: document,
          body: DSL
        })
      );
      this.log({
        tick,
        document,
        body: DSL,
        operation: Operations.getName("SEARCH")
      });
      return resp;
    } catch (err) {
      this.log({
        err,
        tick,
        document,
        body: DSL,
        operation: Operations.getName("SEARCH")
      });
      throw err;
    }
  }

  /**
   * This method is used to scroll on searched data
   *
   * @param {String} scrollId
   * @param {String} aliveTime
   */
  async _scroll(scrollId, aliveTime) {
    const tick = new Timer();
    tick.start();
    const body = {
      scrollId,
      scroll: aliveTime
    };
    try {
      const resp = await this.sendRequest(this.client.scroll(body));
      this.log({
        tick,
        body,
        operation: Operations.getName("SCROLL")
      });
      return resp;
    } catch (err) {
      this.log({
        err,
        tick,
        body,
        operation: Operations.getName("SCROLL")
      });
      throw err;
    }
  }

  /**
   * This method is used to clear scroll context
   *
   * @param {String} scrollId
   */
  async _clearScroll(scrollId) {
    const tick = new Timer();
    tick.start();
    const body = { scrollId: [scrollId] };
    try {
      const resp = await this.sendRequest(this.client.clearScroll(body));
      this.log({
        tick,
        body,
        operation: Operations.getName("CLEAR_SCROLL")
      });
      return resp;
    } catch (err) {
      this.log({
        err,
        tick,
        body,
        operation: Operations.getName("CLEAR_SCROLL")
      });
      throw err;
    }
  }

  /**
   * This method is used to open index
   *
   */
  async open() {
    const tick = new Timer();
    tick.start();
    try {
      const resp = await this.sendRequest(
        this.client.indices.open({
          index: this.indexName
        })
      );
      this.log({
        tick,
        operation: Operations.getName("OPEN_INDEX")
      });
      return resp;
    } catch (err) {
      this.log({
        err,
        tick,
        operation: Operations.getName("OPEN_INDEX")
      });
      throw err;
    }
  }

  /**
   * This method is used to close index
   *
   */
  async close() {
    const tick = new Timer();
    tick.start();
    try {
      const resp = await this.sendRequest(
        this.client.indices.close({
          index: this.indexName
        })
      );
      this.log({
        tick,
        operation: Operations.getName("CLOSE_INDEX")
      });
      return resp;
    } catch (err) {
      this.log({
        err,
        tick,
        operation: Operations.getName("CLOSE_INDEX")
      });
      throw err;
    }
  }

  /**
   * This method is used to query data
   *
   * @param {String} document
   * @param {Object|QueryBuilder} DSL
   * @param {Object} [options={}]
   */
  async search(document, DSL, options = {}) {
    let dsl = (DSL instanceof QueryBuilder) ? DSL.getDSL() : DSL;
    return await this._search(document, dsl, options);
  }

  /**
   * This method is used to search and scroll
   *
   * @param {String} document
   * @param {Object|QueryBuilder} DSL
   * @param {Number} [totalSize=0]
   * @param {String} [aliveTime='3m']
   * @param {String} [options={}]
   */
  async scrolledSearch(document, DSL, totalSize = 0, aliveTime = "3m", options = {}) {
    const hasCallback = typeof (options.scrollCallback) === "function";
    const size = totalSize && this.scrollSize > totalSize ? totalSize : this.scrollSize;
    aliveTime = aliveTime || "3m";
    let fetchedRecordCounts = 0;
    let dsl = (DSL instanceof QueryBuilder) ? DSL.getDSL() : DSL;
    if (!dsl.size) {
      dsl.size = size;
    }
    let resp = await this._search(document, dsl, {
      scroll: aliveTime
    });
    fetchedRecordCounts =  resp.hits.hits.length;
    let scrollId = resp._scroll_id;
    let result = hasCallback ? [] : resp.hits.hits;
    let callbackResult;
    if(hasCallback){
      try{
        callbackResult = await options.scrollCallback(resp.hits.hits);
        result.push(callbackResult)
      }
      catch(e){
        await this._clearScroll(scrollId);
        throw e
      }
    }
    if (resp.hits.total === fetchedRecordCounts) {
      await this._clearScroll(scrollId);
      return result;
    }
    const maxPossibleSize = totalSize && resp.hits.total > totalSize ? totalSize : resp.hits.total;
    let remainingSize = maxPossibleSize - fetchedRecordCounts
    let scroll = remainingSize > 0;
    while (scroll) {
      try {
        let resp = await this._scroll(scrollId, aliveTime);
        fetchedRecordCounts +=  resp.hits.hits.length;
        scrollId = resp._scroll_id;
        if(hasCallback){
          callbackResult = await options.scrollCallback(resp.hits.hits);
          result.push(callbackResult)
        } else {
          result = result.concat(_.take(resp.hits.hits, remainingSize));
        }
        remainingSize = maxPossibleSize - fetchedRecordCounts;
        scroll = remainingSize > 0;
      } catch (e) {
        scroll = false;
        break;
      }
    }
    await this._clearScroll(scrollId);
    return result;
  }

  /**
   * This method is used to index document
   *
   * @param {String} document
   * @param {Object} record
   * @param {String} [uniqueId=null]
   * @param {String} [parentId=null]
   * @param {String} [routingId=null]
   */
  async index(document, record, uniqueId = null, parentId = null, routingId = null) {
    const tick = new Timer();
    tick.start();
    try {
      const resp = await this.sendRequest(
        this.client.index({
          index: this.indexName,
          type: document,
          id: uniqueId || undefined,
          parent: parentId || undefined,
          routing: routingId || parentId || undefined,
          refresh: this.waitForRefresh ? "wait_for" : "false",
          versionType: !this.versionIncrement ? "force" : undefined,
          version: !this.versionIncrement ? 1 : undefined,
          body: record
        })
      );
      this.log({
        tick,
        document,
        body: record,
        operation: Operations.getName("INDEX")
      });
      return resp;
    } catch (err) {
      this.log({
        err,
        tick,
        document,
        body: record,
        operation: Operations.getName("INDEX")
      });
      throw err;
    }
  }

  /**
   * This method is used to update document
   *
   * @param {String} document
   * @param {Object} body
   * @param {String} [uniqueId]
   * @param {String} [parentId=null]
   * @param {String} [routingId=null]
   */
  async update(document, body, uniqueId, parentId = null, routingId = null) {
    const tick = new Timer();
    tick.start();
    try {
      const resp = await this.sendRequest(
        this.client.update({
          index: this.indexName,
          type: document,
          id: uniqueId,
          parent: parentId || undefined,
          routing: routingId || parentId || undefined,
          refresh: this.waitForRefresh ? "wait_for" : "false",
          versionType: !this.versionIncrement ? "force" : undefined,
          version: !this.versionIncrement ? 1 : undefined,
          body
        })
      );
      this.log({
        tick,
        document,
        body,
        operation: Operations.getName("UPDATE")
      });
      return resp;
    } catch (err) {
      this.log({
        err,
        tick,
        document,
        body,
        operation: Operations.getName("UPDATE")
      });
      throw err;
    }
  }

  /**
   * This method is used to delete document
   *
   * @param {String} document
   * @param {String} uniqueId
   * @param {String} [parentId=null]
   * @param {String} [routingId=null]
   */
  async delete(document, uniqueId, parentId = null, routingId = null) {
    const tick = new Timer();
    tick.start();
    try {
      const resp = await this.sendRequest(
        this.client.delete({
          index: this.indexName,
          type: document,
          id: uniqueId,
          parent: parentId || undefined,
          routing: routingId || parentId || undefined,
          refresh: this.waitForRefresh ? "wait_for" : "false",
          versionType: !this.versionIncrement ? "force" : undefined,
          version: !this.versionIncrement ? 1 : undefined
        })
      );
      this.log({
        tick,
        document,
        operation: Operations.getName("DELETE")
      });
      return resp;
    } catch (err) {
      this.log({
        err,
        tick,
        document,
        operation: Operations.getName("DELETE")
      });
      throw err;
    }
  }

  /**
   * This method is used to update documents by query
   *
   * @param {String} document
   * @param {Object|QueryBuilder} query
   * @param {Object} body
   */
  async updateByQuery(document, query, body) {
    const tick = new Timer();
    tick.start();
    if (query instanceof QueryBuilder) {
      query = query.getDSL().query;
    }
    const _body = {
      ...body,
      query
    };
    try {
      const resp = await this.sendRequest(
        this.client.updateByQuery({
          index: this.indexName,
          type: document,
          refresh: true,
          body: _body
        })
      );
      this.log({
        tick,
        document,
        body: _body,
        operation: Operations.getName("UPDATE_BY_QUERY")
      });
      return resp;
    } catch (err) {
      this.log({
        err,
        tick,
        document,
        body: _body,
        operation: Operations.getName("UPDATE_BY_QUERY")
      });
      throw err;
    }
  }

  /**
   * This method is used to delete documents by query
   *
   * @param {String} document
   * @param {Object|QueryBuilder} query
   * @param {Object} [body={}]
   */
  async deleteByQuery(document, query, body = {}) {
    const tick = new Timer();
    tick.start();
    if (query instanceof QueryBuilder) {
      query = query.getDSL().query;
    }
    const _body = {
      ...body,
      query
    };
    try {
      const resp = await this.sendRequest(
        this.client.deleteByQuery({
          index: this.indexName,
          type: document,
          refresh: true,
          body: _body
        })
      );
      this.log({
        tick,
        document,
        body: _body,
        operation: Operations.getName("DELETE_BY_QUERY")
      });
      return resp;
    } catch (err) {
      this.log({
        err,
        tick,
        document,
        body: _body,
        operation: Operations.getName("DELETE_BY_QUERY")
      });
      throw err;
    }
  }

  /**
   * This method is used to index documents in bulk
   *
   * @param {String} document
   * @param {Array} records [Array of documents]
   * @param {String} [uniqueKey=null]
   * @param {String} [parentKey=null]
   * @param {String} [routingKey=null]
   */
  async bulkIndex(document, records, uniqueKey = null, parentKey = null, routingKey = null) {
    var collection = [];
    const version = !this.versionIncrement ? 1 : undefined;
    const versionType = !this.versionIncrement ? "force" : undefined;
    for (var i = 0; i < records.length; i++) {
      var record = records[i];
      if (uniqueKey && !record[uniqueKey]) {
        throw new Error(`Unique key missing on record #${i + 1}`);
      }
      if (parentKey && !record[parentKey]) {
        throw new Error(`Parent key missing on record #${i + 1}`);
      }
      if (routingKey && !record[routingKey]) {
        throw new Error(`Routing key missing on record #${i + 1}`);
      }
      var routingId;
      if (routingKey) {
        routingId = record[routingKey];
      } else if (parentKey) {
        routingId = record[parentKey];
      }
      var obj = {
        _index: this.indexName,
        _type: document,
        _id: record[uniqueKey] || undefined,
        _parent: record[parentKey] || undefined,
        _routing: routingId,
        _version: version,
        _version_type: versionType
      };
      collection.push({
        index: obj
      });
      collection.push(record);
    }
    await this.bulk(collection);
  }

  /**
   * This method is used to update documents in bulk
   *
   * @param {String} document
   * @param {Array} records [keys in each obj: id, parentId, routing, body]
   */
  async bulkUpdate(document, records) {
    var collection = [];
    const version = !this.versionIncrement ? 1 : undefined;
    const versionType = !this.versionIncrement ? "force" : undefined;
    for (var i = 0; i < records.length; i++) {
      var record = records[i];
      if (!record.id) {
        throw new Error(`Key "id" missing in one or more records`);
      }
      if (record.parentId && !record.routing) {
        record.routing = record.parentId;
      }
      var obj = {
        _index: this.indexName,
        _type: document,
        _id: record.id,
        _version: version,
        _version_type: versionType
      };
      if (record.parentId) {
        obj._parent = record.parentId;
      }
      if (record.routing) {
        obj._routing = record.routing;
      }
      collection.push({
        update: obj
      });
      collection.push(record.body);
    }
    await this.bulk(collection);
  }

  /**
   * This method is used to apply bulk ES Operation
   * @param {Array} body
   */
  async bulk(body) {
    const tick = new Timer();
    tick.start();
    try {
      const resp = await this.sendRequest(
        this.client.bulk({
          body,
          refresh: this.waitForRefresh ? "wait_for" : "false"
        })
      );
      if (resp.errors && resp.items) {
        let errorIndex = 0;
        for (var i = 0; i <= resp.items.length; i++) {
          if (resp.items[i].index.status >= 400) {
            errorIndex = i;
            break;
          }
        }
        throw {
          message: "Failed to index document",
          stack: JSON.stringify(resp.items[errorIndex]),
          data: body
        };
      }
      this.log({
        tick,
        body,
        document: body.length >= 2 ? body[1]._type : undefined,
        operation: Operations.getName("BULK")
      });
    } catch (err) {
      this.log({
        err,
        tick,
        body,
        document: body.length >= 2 ? body[1]._type : undefined,
        operation: Operations.getName("BULK")
      });
      throw err;
    }
  }

  /**
   * This method is used to monitor connection status
   */
  async monitorConnection() {
    try {
      await this.ping();
      if (!this.connected && this.debug) {
        console.log(`Connection ("${this.name}") established with elasticsearch`);
      }
      this.connected = true;
    } catch (err) {
      console.log(`Failed to establish connection ("${this.name}") with elasticsearch`);
      this.connected = false;
    } finally {
      await this.sleep(this.reconnectInterval);
      await this.monitorConnection();
    }
  }

  sleep(timeout) {
    return new Promise(resolve => setTimeout(resolve, timeout));
  }

  /**
   *
   * This method is used to log ES operations
   * @param {Object} payload
   * @param {String} payload.operation
   * @param {String} [payload.document]
   * @param {Object} [payload.body]
   * @param {Object} [payload.tick]
   * @param {Object} [payload.err]
   */
  log(payload) {
    if (this.debug) {
      console.info("========================================================================");
      console.info(` Index: ${this.indexName}`);
      console.info(` Operation: ${payload.operation}`);
      if (payload.document) {
        console.info(` Document Type: ${payload.document}`);
      }
      if (payload.tick) {
        console.info(` Time Taken: ${payload.tick.getExecutionTime()}`);
      }
      console.info(` Status: ${!payload.err ? "Success" : "Failure"}`);
      if (payload.body) {
        console.info(` Body: ${JSON.stringify(payload.body, null, 2)}`);
      }
      if (payload.err) {
        if (payload.err.message) {
          console.info(` Error Message: ${payload.err.message}`);
        }
        if (payload.err.stack) {
          console.info(` Error Stack Trace: ${payload.err.stack}`);
        }
      }
      console.info("========================================================================");
    }
  }
}

module.exports = Connection;
